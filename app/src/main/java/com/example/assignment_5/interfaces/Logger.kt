package com.example.assignment_5.interfaces

import android.content.Context
import android.widget.Toast

interface Logger {
    fun callToast(ctx:Context, msg: String){
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show()
    }
}