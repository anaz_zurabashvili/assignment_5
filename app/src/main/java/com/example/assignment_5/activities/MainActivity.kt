package com.example.assignment_5.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.example.assignment_5.interfaces.Logger
import com.example.assignment_5.R
import com.example.assignment_5.databinding.ActivityMainBinding

class MainActivity: AppCompatActivity(), Logger {
    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        binding.info.text = "Welcome ${intent.getStringExtra("email")}!"
        binding.logOutButton.setOnClickListener(logOutButton)
    }

    private val logOutButton = View.OnClickListener {
        callToast(this, getString(R.string.logout_success))
        startActivity(Intent(null, LoginActivity::class.java))
        finish()
    }
}
