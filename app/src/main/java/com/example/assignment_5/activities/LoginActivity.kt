package com.example.assignment_5.activities

import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import com.example.assignment_5.interfaces.Logger
import com.example.assignment_5.R
import com.example.assignment_5.databinding.ActivityLogInBinding
import java.util.regex.Pattern

class LoginActivity : AppCompatActivity(), Logger {
    private lateinit var binding: ActivityLogInBinding
    private val passwordPattern: Pattern by lazy {
        Pattern.compile(
            "^" +
                    "(?=.*[a-z])" +         //at least 1 lower case letter
                    "(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=\\S+$)" +           //without spaces
                    ".{4,}" +               //character >= 4
                    "$"
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLogInBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        binding.logInButton.setOnClickListener(loginButton)
    }

    private fun isAllFieldsValid(): Boolean =
        !(binding.emailAddress.text.isBlank() ||
                binding.password.text.isBlank())
    

    private fun isEmailValid(email: String): Boolean =
        android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()

    private fun isPasswordValid(password: String): Boolean =
        passwordPattern.matcher(password).matches()

    private fun makeValidation(): Boolean =
        isAllFieldsValid() && isEmailValid(binding.emailAddress.text.toString()) &&
                isPasswordValid(binding.password.text.toString())

    private fun getErrorMessage() {
        binding.info.text = getString(R.string.error)
        binding.info.setTextColor(Color.RED)
    }

    private val loginButton = View.OnClickListener {
        if (makeValidation()) {
            callToast(this, getString(R.string.login_success))
            var intent = Intent(this, MainActivity::class.java).apply {
                putExtra("email", binding.password.text.toString())
            }
            startActivity(intent)
        } else {
            getErrorMessage()
            callToast(this, getString(R.string.error))
        }

    }
}